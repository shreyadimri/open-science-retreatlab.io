---
permalink: /registration/
layout: splash
header:
  overlay_color: "#5e616c"
  overlay_image: /assets/images/lake.jpg
author_profile: false
title: "Registration"
---

For application and registration please go to our 
registration platform:

[Registration Platform](https://events.hifis.net/event/622){: .btn .btn--success .btn--large}

## Deadlines

- Event Application: **December 18th 2022**
- Application for Travel Stipends: **December 18th 2022**


## Event Application:

We are organising this event for the first time, have limited space and have no
idea how many people are interested in participating. We also want the event to
be inclusive and diverse. Thus we've decided to use short application forms. 


## Travel Stipends

Our [sponsors](/#sponsors) – Forschungszentrum Jülich, the LMU Open Science Center, and
Helmholtz Open Science – give us the opportunity to hand out a limited amount
of travel stipends. These will cover travel expenses and the costs listed below.


## Prices

Prices include stay, food, coffee breaks, and participation in the event.

| Room type   | Price per Person |
| ----------- | ---------------- |
| Single      | 583 EUR          |
| Shared      | 477 EUR          |

The social activity may come with additional costs, but we will let 
you know once we have planned that out.   


