---
permalink: /participants/
layout: splash
header:
  overlay_color: "#05bf85"
author_profile: false
title: "Participants"
---

*All participants who agreed to be listed online are listed below.*

### [Maroussia Bednarkiewicz](https://uni-tuebingen.de/fakultaeten/philosophische-fakultaet/fachbereiche/asien-orient-wissenschaften/orient-islamwissenschaft/abteilung/personen/maroussia-bednarkiewicz/)  
 I am a historian, linguist and digital enthusiast who meanders between the Humanities and the edges of Computer Sciences.  
 
### [Eduarda Centeno](https://www.linkedin.com/in/eduardagzc/)  
 I am Brazilian, doing a Ph.D. student in Neuroscience (Neurocampus - Université de Bordeaux) and working in parallel as a Research Assistant for a research team in Amsterdam (MULTINET - Vrije Universiteit Medical Center). Both my Ph.D. and RA work is directly linked with Open Science. In my Ph.D., I am developing and using open-source research software, while in my RA job, I am responsible for revising and sharing all our programming scripts (on GitHub). In addition, I lead an OS working group at the VUmc to make our department more in line with OS practices/values, and, in Bordeaux, I host a ReproducibiliTea journal club!    Other than programming, neuroscience, and OS, I also really like cats, cooking, and traveling!  
 
### [Jens Dierkes](https://www.linkedin.com/in/jens-dierkes-2681ab74)  
 I am an astrophysicist turned into a research supporter at a university library. I am curious, interested in the sociotechnical complexities of e-research. I am helping to build bridges between disciplines in terms of data strategies. Currently, I have a focus on personal health data in NFDI4Health and particular on the community and networking aspects. Transparency and FAIR data infrastructure are key elements. Also there is a strong uptake of open science topics locally at the university that I think are very interesting to engage with.  
 
### [Shreya Dimri](Twitter: https://www.twitter.com/shreya_dimri_ )  
 I am a PhD Student with broad interest in the field of behavioural ecology and evolution. My current research is focused on conducting systematic reviews and meta-analyses to better understand individual variation in behavioural traits within and between different species. Additionally, I am also highly interested in meta-science projects, particularly in the reproducibility in ecology and evolution. As a passionate advocate of open science and a continous learner, I believe science is a cumulative and collaborative process that requires participation and responsibility of all involved.
 
### [Meag Doherty](https://twitter.com/EmDohh)  
 I am currently working at the intersection of scientific computing, user experience, and health equity. My day job is to build user experience capacity at the All of Us Research Program at the National Institutes of Health in the U.S.     I come to open science by way of mentoring on Mozilla Open Leaders Training, Open Life Sciences, and a fellowship with the Software Sustainability Institute. I also have some experience in open-source policymaking and program development.  
 
### [Anja Eggert](https://anja-eggert.net/)  
 I'm statistical advisor in a research institute near Rostock and founder of the regional scientific network ORDS (Open and Reproducible Data Science and Statistics). I like to teach, to network and to build diverse teams.  
 
### [Yomna Eid](linkedin.com/in/yomna-eid/)  
 I am a Research Associate at ifgi in WWU Münster, working within the context of the NFDI4Earth project that aims at creating an open science community for Earth System Sciences. I am also pursuing my doctoral studies, and would love to learn about the reproducibility practices as well as get involved in the forward-thinking open science movement.  
 
### [Danny Garside](dannygarside.co.uk)  
 I'm a computational neuroscientist and meta-scientist, splitting my time between modelling colour vision and contributing to open-source projects that aim to make academia more accessible, more efficient, and happier.  
 
### [Monica Gonzalez-Marquez](https://twitter.com/aeryn_thrace)  
 My background is in linguistics, phil of science and experimental psychology. My cultural background is mixed and I'm a woman from a minoritized, yet ancient culture. These factors all contribute to a vision of science where knowledge is a human endowment and a collective responsibility, not just by and for the wealthy global north, but a responsibility we must all share in.  
 
### [Chris Hartgerink](https://chjh.nl)  
 Founder and Director of Liberate Science GmbH (est. 2019). Scientist turned businessperson to build better knowledge systems.  
 
### [Felix Henninger](https://felixhenninger.com)  
 I'm a researcher and research software engineer, supporting others in adopting best practices by making better science easier, providing tools, spreading knowledge, and reducing barriers to adoption. As a trained experimental psychologist and budding statistician, I focus on reproducibility in the data collection, analysis and archival pipeline.  
 
### [Rachel Heyard](https://rachelhey.github.io/)  
 I am a meta-science postdoc at the Center for Reproducible Science at Uni Zurich. I am training PhD Students and other Postdocs in Good Research Practices. My research is centered around measuring replication success, but I am also interested in improving research evaluation and changing incentives. Before starting my postdoc I was a statistician at the Swiss National Science Foundation where I got involved in many meta-science projects.  
 
### [Melanie Imming](meliimming@akademienl.social)  
 As an independent consultant from the Netherlands with a strong international network, I help leading cultural and academic organisations develop their strategy in the area of Open Science and Digital Cultural Heritage. 
 
### [Joyce Kao](https://www.linkedin.com/in/joyceykao/)  
 Currently, I am a Senior Project Manager at the Innovation Center for Digital Medicine at the University Hospital RWTH Aachen (Germany) where I manage projects ranging from clinical app development to digitalization education. Previously, I was a  Co-founder of the Open Innovation in Life Sciences Association, a non-profit entirely operated by early career researchers promoting the practice of open science in Switzerland. I like to help people work more efficiently and creating sustainable communities using digital tools & technologies.  
 
### [Harini Lakshminarayanan](https://www.linkedin.com/in/harini-lakshminarayanan-960772a9/)  
   
 
### [Maxence Larrieu](https://twitter.com/ml4rrieu)  
 I am involved in open science for 8 years, mainly in the open access branch (open repository, diamont open access publishing). I am really intrested in the funding of open infrastructure, bibliometry and research assessment.  
 
### [Kjong Lehmann]()  
   
 
### [Finn Lübber](https://twitter.com/FinnLuebber)  
 I am a PhD student in psychoneuroimmunology, so I am interested in why Descartes was wrong about the idea of dualism. Specifically, I am interested in the impact of (misdirected) immune processes on health, particularly fatigue symptoms, and cognitive processes such as learning ability. I am also an active founding member of the Open Science Initiative Lübeck.  
 
### [Tieu-Binh Ly]()  
   
 
### [Christian Meesters](https://fediscience.org/@rupdecat)
 Computational Scientist at the HPC-Group in Mainz, Germany. Biophysicist by training. PostDoc in Genetic Epidemiology.  Active to promote the quality of FOSS in the sciences.  
 
### [Esther Plomp](https://estherplomp.github.io/)  
 Esther is an Open Science enthusiast. She likes to contribute to a more equitable way of knowledge generation and facilitate others in working more transparently. Esther's research interests are osteology/bioarchaeology. She likes to listen/read books, dance, and draw (when she's not eating pie with friends/family).  
 
### [Franz Poeschel](https://github.com/franzpoeschel)  
 I am a computer scientist who has joined the CASUS institute after gaining his M.Sc. degree at TU Dresden. I work in the intersection of computer science and physics, focusing on scalable and open scientific data.  
 
### [Tom Ratz](tomratz.weebly.com)  
 I'm a Alexander-von-Humboldt Fellow based at the Department of Biology II at LMU. I'm interested in social behaviour in animals and particularly how social interactions affect responses to environmental change.  
 
### [David Schäfer]()  
   
 
### [Ines Schmahl]()  
 I'm a research data manager and a subject librarian. In my daily work I support researchers with their data, information and knowledge management, which is needed for Open Science.  
 
### [Alexander Schniedermann](https://twitter.com/ASchniedermann)  
 I consider myself a metascientists who uses scientoemtrics and qualitative methods to understand open science related innovations in scientific publishing, especially reporting guidelines for systematic reviews in biomedicine.     Although trained in philosophy, I appreciate data driven approaches and enjoy coding where I would love to become more proficient in more holistic software development.     Beside this, I like cooking, running, podcasts, playing guitar and videogames and of course hiking ;-)  
 
### [Lennart Schüler](https://github.com/LSchueler)  
 I am a postdoc in the research data management team at the UFZ. Currently, I am setting up a database for a COVID related project. This includes data pipelines, a Web API for access, and wrappers in different programming languages for programmatic access to the data. As a side project, I am one of the authors of the open geostatistical software package GSTools.  
 
### [Heidi Seibold](https://heidiseibold.com/)  
 I help researchers make their work more open and reproducible. As a solopreneur I give workshops, consult research teams, moderate events, host a podcast (https://anchor.fm/reboot-academia) and write a newsletter (https://heidiseibold.ck.page/).  
 
### [Karan Shah](https://karan.sh)  
 I am a PhD student in the Matter under Extreme Conditions Department at CASUS, under the supervision of Dr. Attila Cangi, funded by Helmholtz AI. I work on physics-informed machine learning to accelerate density functional theory-based electronic structure calculations. I also create educational material to incorporate machine learning and data science topics into the undergraduate physics curriculum.  
 
### [Inês Silva](https://github.com/ecoisilva)  
 I am a postdoctoral researcher in the Earth System Science group at the Center for Advanced Systems Understanding (CASUS), located in Görlitz, Germany. I have a background spanning animal movement, road ecology and community ecology projects in both the neotropical and the paleotropical regions. I am particularly interested in how animal movement is influenced by anthropogenic impacts (such as wildlife-road interactions and wildlife-vehicle collisions), study design and facilitating the uptake of new methods in movement ecology.  
 
### [Álvaro Tejero Cantero](https://mlcolab.org/introml-workshop-july-2022/our-team/lvaro-tejero-cantero)  
 I lead the Machine Learning ⇌ Science Colaboratory (https://mlcolab.org) where we work to empower researchers for discovery with ML. I have been always been obsessed by the interactions between the intellectual property system and the production of public goods such as open source software and learned publications. My background is in theoretical physics and computational neuroscience.  
 
### [Adina Svenja Wagner](https://www.adina-wagner.com)  
 Hi, I'm Adina. I'm working at the Research Center Jülich on software and workflows for data management and reproducible science, and I'm pursuing a PhD in neuroscience at the Heinrich Heine University Düsseldorf. Even though I'm a psychologist by training, the world of open source software is my passion, and the open science community my scientific home. Even though I view many aspects of traditional scientific conduct critically, right now, with scientific software development, I feel that found myself a niche in science that allows me to work on things I find meaningful, sustainable, and helpful for others.  
 
### [Damar Wicaksono]()  
   
 
### [Wino Wijnen](https://www.linkedin.com/in/wino-wijnen-6b068a7b/)  
 Wino has over 15 years of experience in academic research and innovation management. Trained as a molecular biologist, he obtained his PhD at the University of Amsterdam (NL) before continuing as a Postdoctoral Researcher and eventually co-groupleader at the University of Zurich (CH).    Since, he has been working as an innovation manager and career consultant in Zurich, mainly focusing on fundraising, project design, and career development strategies for individual scientists and research consortia.  
 
### [Quirin Würschinger](https://www.anglistik.uni-muenchen.de/personen/wiss_ma/wuerschinger/index.html)  
 I am a research assistant in (computational) linguistics at LMU Munich.
