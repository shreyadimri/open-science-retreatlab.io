---
permalink: /coc/
layout: splash
header:
  overlay_color: "#05bf85"
author_profile: false
title: "Code of conduct"
---

# Code of Conduct
The organizers of Open Science Retreat are dedicated to providing a harassment-free retreat experience for everyone regardless of age, gender, sexual orientation, disability, physical appearance, race, or religion (or lack thereof).

All participants (including attendees, speakers, sponsors and volunteers) at Open Science Retreat are required to agree to the following code of conduct.


The code of conduct applies to all retreat activities including talks, panels, workshops, and social events. It extends to retreat-specific exchanges on social media, for instance, posts tagged with the identifier of the retreat (e.g. #OpenScienceRetreat23 on Twitter) and replies to such posts.

Organizers will enforce this code throughout and expect cooperation in ensuring a safe environment for all.


## Expected Behaviour
All retreat participants agree to:

- Be considerate in language and actions, and respect the boundaries of fellow participants.
- Refrain from demeaning, discriminatory, or harassing behaviour and language. Please refer to Unacceptable Behavior for more details.
- Alert a member of the staff or organizing team, if you notice someone in distress, or observe violations of this code of conduct, even if they seem inconsequential. Please refer to the section titled, What To Do If You Witness or Are Subject To Unacceptable Behavior for more details.


## Unacceptable Behavior
Behaviour that is unacceptable includes, but is not limited to:

- Stalking
- Deliberate intimidation
- Unwanted photography or recording
- Sustained or willful disruption of talks or other events
- Use of sexual or discriminatory imagery, comments, or jokes
- Offensive comments related to age, gender, sexual orientation, disability, race or religion
- Inappropriate physical contact, which can include grabbing, massaging or hugging without consent.
- Unwelcome sexual attention, which can include inappropriate questions of a sexual nature, asking for sexual favours or repeatedly asking for dates or contact information.

If you are asked to stop harassing behaviour you should stop immediately. Even if your behaviour was meant to be friendly or a joke, it was clearly not taken that way and for the comfort of all retreat attendees you should stop.

Attendees who behave in a manner deemed inappropriate are subject to actions listed under Procedure for Code of Conduct Violations.

## Additional Requirements for Retreat Contributions
Presentation slides and posters should not contain offensive or sexualised material. If this material is impossible to avoid given the topic (for example text mining of material from hate sites) the existence of this material should be noted in the abstract and, in the case of oral contributions, at the start of the talk or session.

## Additional Requirements for Sponsors
Sponsors should not use sexualized images, activities, or other material. Booth staff (including volunteers) should not use sexualised clothing/uniforms/costumes or otherwise create a sexualised environment. In case of violations, sanctions may be applied without the return of sponsorship contribution.

## Procedure for Code of Conduct Violations
The organizing committee reserves the right to determine the appropriate response for all code of conduct violations. Potential responses include:

- a formal warning to stop harassing behaviour
- expulsion from the retreat
- removal of sponsor displays or retreat posters
- cancellation or early termination of talks or other contributions to the program
- Refunds may not be given in case of expulsion.

## What To Do If You Witness or Are Subject To Unacceptable Behavior
If you are being harassed, notice that someone else is being harassed, or have any other concerns relating to harassment, please contact a member of the Open Science Retreat staff.

We will take all good-faith reports of harassment by Open Science Retreat participants seriously. This includes harassment outside our spaces and harassment that took place at any point in time. The Open Science Retreat organizing committee reserves the right to exclude people from the Open Science Retreat based on their past behaviour, including behaviour outside of the Open Science Retreat spaces and behaviour towards people who are not part of the Open Science Retreat.

We reserve the right to reject any report we believe to have been made in bad faith. This includes reports intended to silence legitimate criticism.

We will respect confidentiality requests for the purpose of protecting victims of abuse. We will not name harassment victims without their affirmative consent. All data is stored securely and access will be limited to the retreat organisers.

The retreat team will also provide support to victims, including, but not limited to:

- Providing an escort within the Open Science Retreat and affiliated events.
- Contacting hotel/venue security or local law enforcement
- Briefing key event staff for response/victim assistance
- And otherwise assisting those experiencing harassment to ensure that they feel safe for the duration of the retreat.


Questions or concerns about the Code of Conduct can be addressed to 
- Tieu Binh Ly
- Heidi Seibold
- Esther Plomp
- Chris Hartgerink


**Acknowledgements** <br>
Parts of the above text are licensed CC BY-SA 4.0. Credit to SRCCON. This code of conduct was based on that developed for useR!2020 which was a revision of the code of conduct used at previous useR!s and also drew from rOpenSci’s code of conduct.


